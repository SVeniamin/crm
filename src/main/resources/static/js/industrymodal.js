$('#editIndustry').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
  var industryid = button.data('industryid')
  var industryname = button.data('indname') // Извлечение информации из данных-* атрибутов
  // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
  // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
  var modal = $(this)
  modal.find('.modal-title').text('Редактировать  ')
  modal.find('.modal-body #indname').val(industryname)
  modal.find('.modal-body #indid').val(industryid)

})

$('#deleteIndustry').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
    var industryid = button.data('industryid') // Извлечение информации из данных-* атрибутов
    // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
    // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
    var modal = $(this)
    modal.find('.modal-title').text('Удалить ')
    modal.find('.modal-body #inddelid').val(industryid)
  })