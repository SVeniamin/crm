$('#editManager').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
  var managerId = button.data('managerid')
  var managerPhone = button.data('managerphone')
  var managerEmail = button.data('manageremail')
  var managerName = button.data('managername') // Извлечение информации из данных-* атрибутов
  // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
  // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
  var modal = $(this)
  modal.find('.modal-title').text('Редактировать  ')
  modal.find('.modal-body #managerid').val(managerId)
  modal.find('.modal-body #managerphone').val(managerPhone)
  modal.find('.modal-body #manageremail').val(managerEmail)
  modal.find('.modal-body #managername').val(managerName)

})

$('#deleteManager').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
    var managerId = button.data('managerid') // Извлечение информации из данных-* атрибутов
    // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
    // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
    var modal = $(this)
    modal.find('.modal-title').text('Удалить ')
    modal.find('.modal-body #managerdelid').val(managerId)
  })