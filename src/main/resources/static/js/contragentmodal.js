$('#editContrAgent').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
  var contrAgentId = button.data('contragentid')
  var contrAgentPerson = button.data('contragentperson')
  var contrAgentPhone = button.data('contragentphone')
  var contrAgentEmail = button.data('contragentemail')
  var contrAgentINN = button.data('contragentinn')
  var contrAgentName = button.data('contragentname') // Извлечение информации из данных-* атрибутов
  // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
  // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
  var modal = $(this)
  modal.find('.modal-title').text('Редактировать  ')
  modal.find('.modal-body #agentperson').val(contrAgentPerson)
  modal.find('.modal-body #agentphone').val(contrAgentPhone)
  modal.find('.modal-body #agentemail').val(contrAgentEmail)
  modal.find('.modal-body #agentinn').val(contrAgentINN)
  modal.find('.modal-body #agentname').val(contrAgentName)
  modal.find('.modal-body #agentid').val(contrAgentId)

})

$('#deleteContrAgent').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
    var contrAgentId = button.data('contragentid') // Извлечение информации из данных-* атрибутов
    // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
    // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
    var modal = $(this)
    modal.find('.modal-title').text('Удалить ')
    modal.find('.modal-body #agentdelid').val(contrAgentId)
  })