$('#editFormat').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
  var formatid = button.data('formatid')
  var formatname = button.data('fmname') // Извлечение информации из данных-* атрибутов
  // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
  // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
  var modal = $(this)
  modal.find('.modal-title').text('Редактировать  ')
  modal.find('.modal-body #format').val(formatname)
  modal.find('.modal-body #fmtid').val(formatid)

})

$('#deleteFormat').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
    var formatid = button.data('formatid') // Извлечение информации из данных-* атрибутов
    // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
    // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
    var modal = $(this)
    modal.find('.modal-title').text('Удалить ')
    modal.find('.modal-body #fmtdelid').val(formatid)
  })