$('#editModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
  var channelid = button.data('chennelid')
  var channelname = button.data('chname') // Извлечение информации из данных-* атрибутов
  // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
  // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
  var modal = $(this)
  modal.find('.modal-title').text('Редактировать  ')
  modal.find('.modal-body #chname').val(channelname)
  modal.find('.modal-body #chid').val(channelid)

})

$('#deleteModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
    var chandelid = button.data('channelid')// Извлечение информации из данных-* атрибутов
    // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
    // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
    var modal = $(this)
    modal.find('.modal-title').text('Удалить ')
    modal.find('.modal-body #chdelid').val(chandelid)
  })