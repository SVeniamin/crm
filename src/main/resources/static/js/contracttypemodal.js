$('#editcontract').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
  var contractTypeid = button.data('contracttypeid')
  var contractTypeName = button.data('contracttypename') // Извлечение информации из данных-* атрибутов
  // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
  // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
  var modal = $(this)
  console.log(contractTypeid)
  modal.find('.modal-title').text('Редактировать  ')
  modal.find('.modal-body #contname').val(contractTypeName)
  modal.find('.modal-body #contid').val(contractTypeid)
})

$('#deletecontract').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Кнопка, что спровоцировало модальное окно
    var contractTypeid = button.data('contracttypeid')// Извлечение информации из данных-* атрибутов
    // Если необходимо, вы могли бы начать здесь AJAX-запрос (и выполните обновление в обратного вызова).
    // Обновление модальное окно Контента. Мы будем использовать jQuery здесь, но вместо него можно использовать привязки данных библиотеки или других методов.
    var modal = $(this)
    modal.find('.modal-title').text('Удалить ')
    modal.find('.modal-body #contdid').val(contractTypeid)
  })