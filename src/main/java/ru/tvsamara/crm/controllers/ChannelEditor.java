package ru.tvsamara.crm.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ru.tvsamara.crm.entity.Channel;
import ru.tvsamara.crm.repository.ChannelRepository;

@Controller
public class ChannelEditor {

	@Autowired
	ChannelRepository repo;
	@RequestMapping("/channellist")
	public ModelAndView channelList() {
		ModelAndView mdl = new ModelAndView("channeList");
		List<Channel> channels = (List<Channel>) repo.findAll();
		mdl.addObject("channels", channels);
		return mdl;
	}
	@RequestMapping(value = "/channeladd", method = RequestMethod.POST)
	public RedirectView channelAdd(@RequestParam(name = "chid") Long id, @RequestParam(name = "chname") String chname ) {
		
		Channel chanel = new Channel();
		chanel.setName(chname);
		if(id > 0 ) {
			chanel.setId(id);
		}
		repo.save(chanel);
		return new RedirectView("/channellist");
	}
	@RequestMapping(value = "/channeldell", method = RequestMethod.POST)
	public RedirectView channelDel(@RequestParam(name = "chdelid") Long id) {
		Optional<Channel> ch = repo.findById(id);
		if(!ch.isEmpty()) {
			repo.deleteById(id);
		}
		
		return new RedirectView("/channellist");
	}
}
