package ru.tvsamara.crm.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ru.tvsamara.crm.entity.Manager;
import ru.tvsamara.crm.repository.ManagerRepository;

@Controller
public class ManagerEditor {

	@Autowired
	ManagerRepository repo;
	@RequestMapping("/managerlist")
	public ModelAndView managerList() {
		ModelAndView mdl = new ModelAndView("managerList");
		List<Manager> managers = (List<Manager>) repo.findAll();
		mdl.addObject("managers", managers);
		return mdl;
	}
	
	@RequestMapping(value = "/manageradd", method = RequestMethod.POST)
	public RedirectView managerAdd(@RequestParam(name = "managername") String managername, @RequestParam(name = "managerphone") String managerphone,
			@RequestParam(name = "manageremail") String manageremail, @RequestParam(name = "managerid") Long id) {
		Manager manager = new Manager();
		manager.setEmail(manageremail);
		manager.setFullName(managername);
		manager.setPhone(managerphone);
		if(id != 0) {
			manager.setId(id);
		}
		repo.save(manager);
		return new RedirectView("/managerlist");
	}
	
	@RequestMapping(value = "/managerdell", method = RequestMethod.POST)
	public RedirectView managerDell(@RequestParam(name = "managerdelid") Long id) {
		Optional<Manager> manager = repo.findById(id);
		if(!manager.isEmpty()) {
			repo.deleteById(id);
		}
		return new RedirectView("/managerlist");
	}
}
