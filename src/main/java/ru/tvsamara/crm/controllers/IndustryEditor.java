package ru.tvsamara.crm.controllers;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ru.tvsamara.crm.entity.Industry;
import ru.tvsamara.crm.repository.IndustryRepository;

@Controller
public class IndustryEditor {

	@Autowired
	IndustryRepository repo;
	
	@RequestMapping("/industrylist")
	public ModelAndView industryList() {
		ModelAndView mdl = new ModelAndView("industryList");
		List<Industry> industryes = (List<Industry>) repo.findAll();
		mdl.addObject("industryes", industryes);
		return mdl;
	}
	@RequestMapping(value = "/industryadd", method = RequestMethod.POST)
	public RedirectView industryAdd(@RequestParam(name = "indid") Long id, @RequestParam(name = "indname") String name, HttpServletRequest reguest) {
		Industry industry = new Industry();
		industry.setName(name);
		if(id != 0) {
			industry.setId(id);
		}
		repo.save(industry);		
		return new RedirectView("/industrylist");
	}
	
	@RequestMapping(value = "/industrydell", method = RequestMethod.POST)
	public RedirectView industryDell(@RequestParam(name = "inddelid") Long id) {
		Optional<Industry> industry = repo.findById(id);
		if(!industry.isEmpty()) {
			repo.deleteById(id);
		}
		
		return new RedirectView("/industrylist");
	}
}
