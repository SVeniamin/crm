package ru.tvsamara.crm.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ru.tvsamara.crm.entity.ContrAgent;
import ru.tvsamara.crm.repository.ContrAgentRepository;

@Controller
public class ContrAgentEditor {
	
	@Autowired
	ContrAgentRepository repo;
	
	@RequestMapping("/contragentlist")
	public ModelAndView contragentList() {
		ModelAndView mdl = new ModelAndView("contrAgentList");
		List<ContrAgent> contragents = (List<ContrAgent>) repo.findAll();
		mdl.addObject("contragents", contragents);
		return mdl;
	}
	
	@RequestMapping(value = "/contragentadd", method = RequestMethod.POST)
	public RedirectView contragentAdd(@RequestParam(name = "agentname") String agentname, @RequestParam(name = "agentperson") String agentperson,
			@RequestParam(name = "agentphone") String agentphone, @RequestParam(name = "agentemail") String agentemail, 
			@RequestParam(name = "agentinn") String agentinn, @RequestParam(name = "agentid") Long id) {
		ContrAgent agent = new ContrAgent();
		agent.setFullName(agentname);
		agent.setContactName(agentperson);
		agent.setPhone(agentphone);
		agent.setEmail(agentemail);
		agent.setInn(agentinn);
		if(id != 0) {
			agent.setId(id);
		}
		repo.save(agent);
		return new RedirectView("/contragentlist");
	}
	
	@RequestMapping(value = "/contragentdell", method = RequestMethod.POST)
	public RedirectView contragentDell(@RequestParam(name = "agentdelid") Long id) {
		Optional<ContrAgent> agent = repo.findById(id);
		if(!agent.isEmpty()) {
			repo.deleteById(id);
		}
		
		return new RedirectView("/contragentlist");
	}
}
