package ru.tvsamara.crm.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ru.tvsamara.crm.entity.Format;
import ru.tvsamara.crm.repository.FormatRepository;

@Controller
public class FormatEditor {

	@Autowired
	FormatRepository repo;
	
	@RequestMapping("/formatlist")
	public ModelAndView formatList() {
		ModelAndView mdl = new ModelAndView("formatList");
		List<Format> formats = (List<Format>) repo.findAll();
		mdl.addObject("formats", formats);
		return mdl;
	}
	@RequestMapping(value = "/formatadd", method = RequestMethod.POST)
	public RedirectView formatAdd(@RequestParam(name = "format") String name, @RequestParam(name = "fmtid") Long id) {
		Format fmt = new Format();
		fmt.setName(name);
		if(id != 0) {
			fmt.setId(id);
		}
		repo.save(fmt);
		return new RedirectView("/formatlist");
	}
	@RequestMapping(value = "/formatdell", method = RequestMethod.POST)
	public RedirectView formatDell(@RequestParam(name = "fmtdelid") Long id) {
		Optional<Format> fmt = repo.findById(id);
		if(!fmt.isEmpty()) {
			repo.deleteById(id);
		}
		return new RedirectView("/formatlist");
	}
}
