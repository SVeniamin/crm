package ru.tvsamara.crm.controllers;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.tvsamara.crm.entity.ContractType;
import ru.tvsamara.crm.repository.ContractTypeRepository;
@Controller
public class ContractTypeEditor {

	@Autowired
	ContractTypeRepository repo;
	
	@RequestMapping(value = "/contracttypelist", method = RequestMethod.GET)
	public ModelAndView contractTypeList() {
		ModelAndView mdl = new ModelAndView("contractTypeList");
		List<ContractType> contractTypes = (List<ContractType>) repo.findAll();
		mdl.addObject("contractsTypes", contractTypes);
		return mdl;
	}
	
	@RequestMapping(value = "/contracttypeadd", method = RequestMethod.POST)
	public RedirectView contractTypeAdd(@RequestParam(value = "contid") Long id, @RequestParam(value = "contname") String name) {
		ContractType contractType = new ContractType();
		contractType.setName(name);
		if(id !=0 ) {
			contractType.setId(id);
		}
		repo.save(contractType);
		return new RedirectView("/contracttypelist");
	}
	
	@RequestMapping(value = "/contracttypedell", method = RequestMethod.POST)
	public RedirectView contractTypeDell(@RequestParam(value = "contdid") Long id) {
		Optional<ContractType> contractType = repo.findById(id);
		if(!contractType.isEmpty()) {
			repo.deleteById(id);
		}
		return new RedirectView("/contracttypelist");
	}
}
