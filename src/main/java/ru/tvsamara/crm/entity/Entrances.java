package ru.tvsamara.crm.entity;

import java.time.LocalDate;



public class Entrances {

	private Long id;
	private Manager manager;
	private LocalDate data;
	private ContrAgent consumer;
	private Brend brend;
	private Industry industry;
	private Format format;
	private Channel channel;
	private LocalDate dateInputPlane;
	private LocalDate dateInputFact;
	private Double inputMany;
	private String comment;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Manager getManager() {
		return manager;
	}
	public void setManager(Manager manager) {
		this.manager = manager;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public ContrAgent getConsumer() {
		return consumer;
	}
	public void setConsumer(ContrAgent consumer) {
		this.consumer = consumer;
	}
	public Brend getBrend() {
		return brend;
	}
	public void setBrend(Brend brend) {
		this.brend = brend;
	}
	public Industry getIndustry() {
		return industry;
	}
	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	public Format getFormat() {
		return format;
	}
	public void setFormat(Format format) {
		this.format = format;
	}
	public Channel getChannel() {
		return channel;
	}
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	public LocalDate getDateInputPlane() {
		return dateInputPlane;
	}
	public void setDateInputPlane(LocalDate dateInputPlane) {
		this.dateInputPlane = dateInputPlane;
	}
	public LocalDate getDateInputFact() {
		return dateInputFact;
	}
	public void setDateInputFact(LocalDate dateInputFact) {
		this.dateInputFact = dateInputFact;
	}
	public Double getInputMany() {
		return inputMany;
	}
	public void setInputMany(Double inputMany) {
		this.inputMany = inputMany;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	

}
