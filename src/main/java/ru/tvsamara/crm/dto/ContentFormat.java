package ru.tvsamara.crm.dto;

import java.util.LinkedList;

public class ContentFormat {
	private LinkedList<String> format;

	public LinkedList<String> getFormat() {
		return format;
	}

	public void setFormat(LinkedList<String> format) {
		this.format = format;
	}
	
}
