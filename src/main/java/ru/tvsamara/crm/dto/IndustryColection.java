package ru.tvsamara.crm.dto;

import java.util.LinkedList;

public class IndustryColection {
	private LinkedList<String> industry;

	public LinkedList<String> getIndustry() {
		return industry;
	}

	public void setIndustry(LinkedList<String> industry) {
		this.industry = industry;
	}
}
