package ru.tvsamara.crm.dto;
/**
 * Каналы
 * @author gtrk
 *
 */

import java.util.LinkedList;

public class Channels {
	private LinkedList<String> channels;

	public LinkedList<String> getChannel() {
		return channels;
	}

	public void setChannel(LinkedList<String> channels) {
		this.channels = channels;
	}
	
}
