package ru.tvsamara.crm.repository;

import org.springframework.data.repository.CrudRepository;

import ru.tvsamara.crm.entity.Manager;

public interface ManagerRepository extends CrudRepository<Manager, Long> {

}
