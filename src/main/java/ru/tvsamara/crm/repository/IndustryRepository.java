package ru.tvsamara.crm.repository;

import org.springframework.data.repository.CrudRepository;

import ru.tvsamara.crm.entity.Industry;

public interface IndustryRepository extends CrudRepository<Industry, Long> {

}
