package ru.tvsamara.crm.repository;

import org.springframework.data.repository.CrudRepository;

import ru.tvsamara.crm.entity.ContrAgent;

public interface ContrAgentRepository extends CrudRepository<ContrAgent, Long> {

}
