package ru.tvsamara.crm.repository;

import org.springframework.data.repository.CrudRepository;

import ru.tvsamara.crm.entity.Channel;

public interface ChannelRepository extends CrudRepository<Channel, Long> {

}
