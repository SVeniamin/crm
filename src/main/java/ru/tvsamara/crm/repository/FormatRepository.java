package ru.tvsamara.crm.repository;

import org.springframework.data.repository.CrudRepository;

import ru.tvsamara.crm.entity.Format;

public interface FormatRepository extends CrudRepository<Format, Long> {

}
