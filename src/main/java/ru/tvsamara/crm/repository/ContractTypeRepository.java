package ru.tvsamara.crm.repository;

import org.springframework.data.repository.CrudRepository;

import ru.tvsamara.crm.entity.ContractType;

public interface ContractTypeRepository extends CrudRepository<ContractType, Long> {

}
